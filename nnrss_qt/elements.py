from datetime import datetime
from typing import List


class FeedEntry:
    def __init__(self, the_id: int, title: str, url: str, date: str, unread: bool):
        self.__id = the_id
        self.__title = title
        self.__url = url
        self.__date = datetime.fromtimestamp(date)
        self.__unread = unread

    @property
    def id(self) -> int:
        return self.__id

    @property
    def title(self) -> str:
        return self.__title

    @property
    def url(self) -> str:
        return self.__url

    @property
    def unread(self) -> bool:
        return self.__unread

    @unread.setter
    def unread(self, status: bool):
        self.__unread = status

    @property
    def date(self) -> datetime:
        return self.__date.strftime("%d %B %Y %I:%M %p")

    def __repr__(self):
        return "FeedEntry: {} {}".format(self.__title, self.__url)

    def __eq__(self, other):
        if self.id == other.id and self.title == other.title and self.url == other.url:
            return True
        return False


class Feed:
    def __init__(self, title: str, url: str, category: str):
        self.__title = title
        self.__url = url
        self.__category = category
        self.__entries = []

    def add_entry(self, the_id: int, title: str, url: str, date: str, unread: bool):
        self.__entries.append(
            FeedEntry(the_id, title, url, datetime.fromtimestamp(date), unread)
        )

    def add_entry_feedentry(self, feedentry: FeedEntry):
        self.__entries.append(feedentry)

    @property
    def entries(self) -> List[FeedEntry]:
        return self.__entries

    @property
    def unread_entries(self) -> List[FeedEntry]:
        return [entry for entry in self.__entries if entry.unread]

    @property
    def url(self) -> str:
        return self.__url

    @property
    def title(self) -> str:
        return self.__title

    @property
    def category(self) -> str:
        return self.__category

    def __repr__(self):
        return "Feed: {} {}".format(self.__title, self.__url)
