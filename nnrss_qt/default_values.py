class DefaultValues:
    default_domain = "http://localhost:5000"
    default_creds = "change_me"
    default_update_time = 30
    all_feeds_filter = "all feeds"
    unread_feeds__filter = "only unread feeds"
