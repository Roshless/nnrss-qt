from pathlib import Path
from pickle import dump, load

from requests import delete
from requests import exceptions as request_exceptions
from requests import get, post

from nnrss_qt.elements import Feed, FeedEntry


class ApiHandler:
    __feeds = {}
    __status = "Connected"
    status_connection_error = "Connection error"
    status_feed_not_found = "Feed not found"
    status_connection_lost = "Connection lost"

    new_entries_counter = 0
    pickle_path = f"{Path.home()}/.config/nnrss_qt/data.pickle"

    def __init__(
        self, domain: str, username: str, api_key: str, update_interval_minutes: int
    ):
        self.url = "{}/api/feed".format(domain)
        self.headers = {"API-KEY": api_key, "USERNAME": username}
        self.update_interval_minutes = update_interval_minutes

        try:
            self.__feeds = self.load_cached()
        except FileNotFoundError:
            pass
        self.__get_feeds()

    def __get_feeds(self):
        try:
            response = get(self.url, headers=self.headers)
        except request_exceptions.ConnectionError:
            self.__status = self.status_connection_lost
            return []
        if response.status_code != 200:
            self.__status = self.status_connection_error
            return []
        self.__feeds = {}
        for feed in response.json():
            self.__feeds[feed["id"]] = Feed(
                feed["title"], feed["link"], feed["category"]
            )

    def __get_entries(self, index, count_entries=False):
        try:
            response = get(index, headers=self.headers)
        except request_exceptions.ConnectionError:
            self.__status = self.status_connection_lost
            return []
        if response.status_code == 404:
            """Feed no longer exists"""
            del self.__feeds[index]
            return []
        elif response.status_code != 200:
            self.__status = self.status_connection_error
            return []
        for feed_entry in response.json():
            """If entry is_read is set to False it means it's unread
            so we have to negate"""
            new_feedentry = FeedEntry(
                feed_entry["id"],
                feed_entry["title"],
                feed_entry["link"],
                feed_entry["date"],
                not feed_entry["is_read"],
            )
            if new_feedentry not in self.__feeds[index].entries:
                self.__feeds[index].add_entry_feedentry(new_feedentry)
                if count_entries:
                    self.new_entries_counter += 1

    def __remove_feed(self, feed_id: int) -> bool:
        try:
            response = delete(feed_id, headers=self.headers)
        except request_exceptions.ConnectionError:
            self.__status = self.status_connection_lost
            return False
        if response.status_code != 200:
            self.__status = response.content.decode("utf-8")
            return False
        del self.__feeds[feed_id]
        self.__status = "Feed removed"
        return True

    def __add_feed(self, feed_link, category) -> bool:
        try:
            response = post(
                self.url,
                params={"url": feed_link, "category": category},
                headers=self.headers,
            )
        except request_exceptions.ConnectionError:
            self.__status = self.status_connection_lost
            return False
        if response.status_code != 200:
            self.__status = response.content.decode("utf-8")
            return False
        self.__status = "Feed added"
        return True

    def __set_read_entry(self, index_feed, index_entry):
        index = list(self.__feeds.keys())[index_feed]
        payload = {"entry_id": index_entry}
        try:
            response = post(index, params=payload, headers=self.headers)
        except request_exceptions.ConnectionError:
            self.__status = self.status_connection_lost
            return []
        if response.status_code != 200:
            self.__status = response.content.decode("utf-8")
            return []

    ###

    def get_feeds(self, force):
        if not self.__feeds or force:
            self.__get_feeds()
        return self.__feeds.items()

    def get_entries(self, index, only_unread=False):
        try:
            index = list(self.__feeds.keys())[index]
        except IndexError:
            """Happens after deleting last feed, selection doesn't go away
            and it still has last feed selected, invokes new event and asks
            for now not existing last feeds index"""
            return None
        if not self.__feeds[index].entries:
            self.__get_entries(index)
        if only_unread:
            return self.__feeds[index].unread_entries
        return self.__feeds[index].entries

    def get_all_entries(self):
        self.new_entries_counter = 0
        for feed_id in list(self.__feeds):
            self.__get_entries(feed_id, count_entries=True)
        return self.new_entries_counter

    def get_link(self, index_feed, index_entries):
        index = list(self.__feeds.keys())[index_feed]
        return self.__feeds[index].entries[index_entries].url

    def get_latest_status(self):
        status = self.__status
        self.__status = None
        return status

    def remove_feed(self, index: int) -> bool:
        feed_id = list(self.__feeds.keys())[index]
        return self.__remove_feed(feed_id)

    def add_feed(self, feed_link, category):
        return self.__add_feed(feed_link, category)

    def set_read(self, index_feed: int, index_entries: int):
        index = list(self.__feeds.keys())[index_feed]
        index_entry = self.__feeds[index].entries[index_entries].id
        self.__set_read_entry(index_feed, index_entry)
        self.__feeds[index].entries[index_entries].unread = False

    def save_cached(self):
        with open(self.pickle_path, "wb") as f:
            dump(self.__feeds, f)

    def load_cached(self):
        with open(self.pickle_path, "rb") as f:
            return load(f)
