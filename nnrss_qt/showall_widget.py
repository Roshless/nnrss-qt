from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QHBoxLayout, QPushButton, QWidget


class QShowAllSwitch(QWidget):
    show_all_clicked = pyqtSignal()
    show_unread_clicked = pyqtSignal()

    def __init__(self, parent=None):
        super(QShowAllSwitch, self).__init__(parent)

        self.layout = QHBoxLayout()
        self.showAllButton = QPushButton()
        self.showAllButton.setText("ALL")
        self.showAllButton.clicked.connect(self.show_all_event)
        self.showAllButton.setEnabled(False)

        self.showUnreadButton = QPushButton()
        self.showUnreadButton.setText("UNREAD")
        self.showUnreadButton.clicked.connect(self.show_unread_event)

        self.layout.addWidget(self.showAllButton)
        self.layout.addWidget(self.showUnreadButton)

        self.setLayout(self.layout)

    def show_all_event(self):
        self.showUnreadButton.setDisabled(False)
        self.showAllButton.setDisabled(True)
        self.show_all_clicked.emit()

    def show_unread_event(self):
        self.showUnreadButton.setDisabled(True)
        self.showAllButton.setDisabled(False)
        self.show_unread_clicked.emit()

    def is_unread_checked(self):
        return self.showAllButton.isEnabled()

    def set_state(self, show_only_unread):
        if show_only_unread:
            self.show_unread_event()
        else:
            self.show_all_event()
