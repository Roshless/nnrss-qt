from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QSettings
from PyQt5.QtWidgets import QWizard

from nnrss_qt.default_values import DefaultValues


class RssWizard(QWizard):
    def setupUi(self, Wizard):
        Wizard.setObjectName("Wizard")
        Wizard.resize(500, 360)
        Wizard.setMinimumSize(QtCore.QSize(500, 360))
        Wizard.setMaximumSize(QtCore.QSize(500, 360))
        Wizard.setSizeGripEnabled(False)
        Wizard.setModal(False)
        Wizard.setWizardStyle(QtWidgets.QWizard.ClassicStyle)
        Wizard.setOptions(QtWidgets.QWizard.NoCancelButton)
        self.wizardPageWelcome = QtWidgets.QWizardPage()
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.wizardPageWelcome.sizePolicy().hasHeightForWidth()
        )
        self.wizardPageWelcome.setSizePolicy(sizePolicy)
        self.wizardPageWelcome.setObjectName("wizardPageWelcome")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.wizardPageWelcome)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.wizardPageWelcome)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        Wizard.addPage(self.wizardPageWelcome)
        self.wizardPageServerSettings = QtWidgets.QWizardPage()
        self.wizardPageServerSettings.setSubTitle("")
        self.wizardPageServerSettings.setObjectName("wizardPageServerSettings")
        self.formLayout = QtWidgets.QFormLayout(self.wizardPageServerSettings)
        self.formLayout.setObjectName("formLayout")
        self.labelURL = QtWidgets.QLabel(self.wizardPageServerSettings)
        self.labelURL.setObjectName("labelURL")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.labelURL)
        self.lineEditURL = QtWidgets.QLineEdit(self.wizardPageServerSettings)
        self.lineEditURL.setText("")
        self.lineEditURL.setObjectName("lineEditURL")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.lineEditURL)
        self.labelUsername = QtWidgets.QLabel(self.wizardPageServerSettings)
        self.labelUsername.setObjectName("labelUsername")
        self.formLayout.setWidget(
            1, QtWidgets.QFormLayout.LabelRole, self.labelUsername
        )
        self.lineEditUsername = QtWidgets.QLineEdit(self.wizardPageServerSettings)
        self.lineEditUsername.setObjectName("lineEditUsername")
        self.formLayout.setWidget(
            1, QtWidgets.QFormLayout.FieldRole, self.lineEditUsername
        )
        self.labelAPIKey = QtWidgets.QLabel(self.wizardPageServerSettings)
        self.labelAPIKey.setObjectName("labelAPIKey")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.labelAPIKey)
        self.lineEditAPIKey = QtWidgets.QLineEdit(self.wizardPageServerSettings)
        self.lineEditAPIKey.setObjectName("lineEditAPIKey")
        self.formLayout.setWidget(
            2, QtWidgets.QFormLayout.FieldRole, self.lineEditAPIKey
        )
        Wizard.addPage(self.wizardPageServerSettings)
        self.wizardPageUserSettings = QtWidgets.QWizardPage()
        self.wizardPageUserSettings.setSubTitle("")
        self.wizardPageUserSettings.setObjectName("wizardPageUserSettings")
        self.formLayout_2 = QtWidgets.QFormLayout(self.wizardPageUserSettings)
        self.formLayout_2.setObjectName("formLayout_2")
        self.labelFeedFilter = QtWidgets.QLabel(self.wizardPageUserSettings)
        self.labelFeedFilter.setObjectName("labelFeedFilter")
        self.formLayout_2.setWidget(
            0, QtWidgets.QFormLayout.LabelRole, self.labelFeedFilter
        )
        self.comboBoxFilter = QtWidgets.QComboBox(self.wizardPageUserSettings)
        self.comboBoxFilter.setMaxVisibleItems(2)
        self.comboBoxFilter.setObjectName("comboBoxFilter")
        self.formLayout_2.setWidget(
            0, QtWidgets.QFormLayout.FieldRole, self.comboBoxFilter
        )
        self.labelUpdateInt = QtWidgets.QLabel(self.wizardPageUserSettings)
        self.labelUpdateInt.setToolTip("")
        self.labelUpdateInt.setObjectName("labelUpdateInt")
        self.formLayout_2.setWidget(
            1, QtWidgets.QFormLayout.LabelRole, self.labelUpdateInt
        )
        self.spinBoxUpdateInt = QtWidgets.QSpinBox(self.wizardPageUserSettings)
        self.spinBoxUpdateInt.setMinimum(5)
        self.spinBoxUpdateInt.setMaximum(960)
        self.spinBoxUpdateInt.setSingleStep(5)
        self.spinBoxUpdateInt.setObjectName("spinBoxUpdateInt")
        self.formLayout_2.setWidget(
            1, QtWidgets.QFormLayout.FieldRole, self.spinBoxUpdateInt
        )
        Wizard.addPage(self.wizardPageUserSettings)
        self.wizardDonePage = QtWidgets.QWizardPage()
        self.wizardDonePage.setSubTitle("")
        self.wizardDonePage.setObjectName("wizardDonePage")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.wizardDonePage)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        Wizard.addPage(self.wizardDonePage)

        self.retranslateUi(Wizard)
        QtCore.QMetaObject.connectSlotsByName(Wizard)

    def retranslateUi(self, Wizard):
        _translate = QtCore.QCoreApplication.translate
        Wizard.setWindowTitle(_translate("Wizard", "Wizard"))
        self.wizardPageWelcome.setTitle(_translate("Wizard", "Hello"))
        self.wizardPageWelcome.setSubTitle(
            _translate("Wizard", "Welcome to initial configuration wizard.")
        )
        self.label.setText(
            _translate(
                "Wizard",
                '<html><head/><body><p align="center"><span style=" font-weight:600;">Remember to enable API in config file!</span></p></body></html>',
            )
        )
        self.wizardPageServerSettings.setTitle(_translate("Wizard", "Server settings"))
        self.labelURL.setText(_translate("Wizard", "Server"))
        self.labelUsername.setText(_translate("Wizard", "Username"))
        self.labelAPIKey.setText(_translate("Wizard", "API key"))
        self.wizardPageUserSettings.setTitle(_translate("Wizard", "User settings"))
        self.labelFeedFilter.setText(_translate("Wizard", "Feed filter"))
        self.labelUpdateInt.setText(_translate("Wizard", "Update feeds"))
        self.spinBoxUpdateInt.setSuffix(_translate("Wizard", " minutes"))
        self.spinBoxUpdateInt.setPrefix(_translate("Wizard", "every "))
        self.wizardDonePage.setTitle(_translate("Wizard", "Done"))

    def __init__(self):
        super(RssWizard, self).__init__()
        self.setupUi(self)

        def_val = DefaultValues()
        self.settings = QSettings()
        self.lineEditURL.setText(
            self.settings.value("domain", def_val.default_domain, str)
        )
        self.lineEditAPIKey.setText(
            self.settings.value("api_key", def_val.default_creds, str)
        )
        self.lineEditUsername.setText(
            self.settings.value("username", def_val.default_creds, str)
        )
        self.spinBoxUpdateInt.setValue(
            self.settings.value("update_interval", def_val.default_update_time, int)
        )
        self.comboBoxFilter.addItems(
            [def_val.all_feeds_filter, def_val.unread_feeds__filter]
        )
        if (
            self.settings.value("feed_filter", def_val.all_feeds_filter, str)
            == def_val.unread_feeds__filter
        ):
            self.comboBoxFilter.setCurrentIndex(1)

        self.button(QWizard.FinishButton).clicked.connect(self.save_settings)

    def save_settings(self):
        self.settings.setValue("first_run", False)
        self.settings.setValue("domain", self.lineEditURL.text())
        self.settings.setValue("username", self.lineEditUsername.text())
        self.settings.setValue("api_key", self.lineEditAPIKey.text())
        self.settings.setValue("update_interval", self.spinBoxUpdateInt.value())
        self.settings.setValue("feed_filter", self.comboBoxFilter.currentText())

        self.settings.sync()
