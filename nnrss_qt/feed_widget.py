from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QGridLayout, QLabel, QSizePolicy, QWidget


class QWidgetFeeds(QWidget):
    def __init__(self, parent=None):
        super(QWidgetFeeds, self).__init__(parent)
        self.text_up_label = QLabel()
        self.text_down_label = QLabel()
        self.text_up_label.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Preferred)
        self.text_down_label.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Preferred)

        self.iconQLabel = QLabel()
        icon = QIcon.fromTheme("insert-link")
        self.iconQLabel.setPixmap(QPixmap((icon.pixmap(24, 24))))
        self.iconQLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.iconQLabel.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)

        self.category_label = QLabel()
        self.category_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        self.category_label.setAlignment(Qt.AlignRight | Qt.AlignVCenter)

        self.layout = QGridLayout()
        self.layout.setHorizontalSpacing(15)
        self.layout.addWidget(self.text_up_label, 0, 0)
        self.layout.addWidget(self.text_down_label, 1, 0)
        self.layout.addWidget(self.iconQLabel, 0, 1)
        self.layout.addWidget(self.category_label, 1, 1)

        self.setLayout(self.layout)
        self.text_up_label.setStyleSheet("font-weight: bold;")
        self.category_label.setStyleSheet("font-style: italic;")

        self.setStyleSheet(
            """
        :selected {
        background-color: red;
        }
        /*:hover {
        background-color: red;
        }
        :hover:selected {
        background-color: red;
        }*/
        """
        )

    def setUpLabel(self, text):
        self.text_up_label.setText(text)

    def setDownLabel(self, text):
        self.text_down_label.setText(text)

    def setCategoryLabel(self, text):
        self.category_label.setText(text)

    def disableIcon(self):
        self.iconQLabel.hide()

    def setReadEntry(self):
        # self.setAttribute(Qt.WA_StyledBackground, True)
        self.setStyleSheet(
            """
              color: white;
              background-color: red;
              selection-background-color: yellow;
        """
        )
