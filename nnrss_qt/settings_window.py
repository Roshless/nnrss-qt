from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QSettings
from PyQt5.QtWidgets import QDialog

from nnrss_qt.default_values import DefaultValues


class SettingsWindow(QDialog):
    def setupUi(self, Settings):
        Settings.setObjectName("Settings")
        Settings.resize(600, 400)
        Settings.setMinimumSize(QtCore.QSize(600, 400))
        Settings.setMaximumSize(QtCore.QSize(650, 400))
        self.verticalLayout = QtWidgets.QVBoxLayout(Settings)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setRowWrapPolicy(QtWidgets.QFormLayout.WrapAllRows)
        self.formLayout.setObjectName("formLayout")
        self.serverLabel = QtWidgets.QLabel(Settings)
        self.serverLabel.setObjectName("serverLabel")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.serverLabel)
        self.serverLineEdit = QtWidgets.QLineEdit(Settings)
        self.serverLineEdit.setObjectName("serverLineEdit")
        self.formLayout.setWidget(
            0, QtWidgets.QFormLayout.FieldRole, self.serverLineEdit
        )
        self.usernameLabel = QtWidgets.QLabel(Settings)
        self.usernameLabel.setObjectName("usernameLabel")
        self.formLayout.setWidget(
            1, QtWidgets.QFormLayout.LabelRole, self.usernameLabel
        )
        self.usernameLineEdit = QtWidgets.QLineEdit(Settings)
        self.usernameLineEdit.setObjectName("usernameLineEdit")
        self.formLayout.setWidget(
            1, QtWidgets.QFormLayout.FieldRole, self.usernameLineEdit
        )
        self.APIKeyLabel = QtWidgets.QLabel(Settings)
        self.APIKeyLabel.setObjectName("APIKeyLabel")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.APIKeyLabel)
        self.APIKeyLineEdit = QtWidgets.QLineEdit(Settings)
        self.APIKeyLineEdit.setObjectName("APIKeyLineEdit")
        self.formLayout.setWidget(
            2, QtWidgets.QFormLayout.FieldRole, self.APIKeyLineEdit
        )
        self.feedFilterLabel = QtWidgets.QLabel(Settings)
        self.feedFilterLabel.setObjectName("feedFilterLabel")
        self.formLayout.setWidget(
            3, QtWidgets.QFormLayout.LabelRole, self.feedFilterLabel
        )
        self.feedFilterComboBox = QtWidgets.QComboBox(Settings)
        self.feedFilterComboBox.setObjectName("feedFilterComboBox")
        self.formLayout.setWidget(
            3, QtWidgets.QFormLayout.FieldRole, self.feedFilterComboBox
        )
        self.updateFeedsLabel = QtWidgets.QLabel(Settings)
        self.updateFeedsLabel.setObjectName("updateFeedsLabel")
        self.formLayout.setWidget(
            4, QtWidgets.QFormLayout.LabelRole, self.updateFeedsLabel
        )
        self.updateFeedsSpinBox = QtWidgets.QSpinBox(Settings)
        self.updateFeedsSpinBox.setMinimum(5)
        self.updateFeedsSpinBox.setObjectName("updateFeedsSpinBox")
        self.formLayout.setWidget(
            4, QtWidgets.QFormLayout.FieldRole, self.updateFeedsSpinBox
        )
        self.buttonBox = QtWidgets.QDialogButtonBox(Settings)
        self.buttonBox.setStandardButtons(
            QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Save
        )
        self.buttonBox.setObjectName("buttonBox")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.buttonBox)
        self.verticalLayout.addLayout(self.formLayout)

        self.retranslateUi(Settings)
        QtCore.QMetaObject.connectSlotsByName(Settings)

    def retranslateUi(self, Settings):
        _translate = QtCore.QCoreApplication.translate
        Settings.setWindowTitle(_translate("Settings", "Settings"))
        self.serverLabel.setText(_translate("Settings", "Server"))
        self.usernameLabel.setText(_translate("Settings", "Username"))
        self.APIKeyLabel.setText(_translate("Settings", "API key"))
        self.feedFilterLabel.setText(_translate("Settings", "Feed filter"))
        self.updateFeedsLabel.setText(_translate("Settings", "Update feeds"))
        self.updateFeedsSpinBox.setSuffix(_translate("Settings", " minutes"))
        self.updateFeedsSpinBox.setPrefix(_translate("Settings", "every "))

    def __init__(self):
        super(SettingsWindow, self).__init__()
        self.setupUi(self)

        def_val = DefaultValues()
        self.settings = QSettings()
        self.serverLineEdit.setText(
            self.settings.value("domain", def_val.default_domain, str)
        )
        self.APIKeyLineEdit.setText(
            self.settings.value("api_key", def_val.default_creds, str)
        )
        self.usernameLineEdit.setText(
            self.settings.value("username", def_val.default_creds, str)
        )
        self.updateFeedsSpinBox.setValue(
            self.settings.value("update_interval", def_val.default_update_time, int)
        )
        self.feedFilterComboBox.addItems(
            [def_val.all_feeds_filter, def_val.unread_feeds__filter]
        )
        if (
            self.settings.value("feed_filter", def_val.all_feeds_filter, str)
            == def_val.unread_feeds__filter
        ):
            self.feedFilterComboBox.setCurrentIndex(1)

        self.buttonBox.accepted.connect(self.save_settings)
        self.buttonBox.rejected.connect(self.close)

    def save_settings(self):
        self.settings.setValue("domain", self.serverLineEdit.text())
        self.settings.setValue("username", self.usernameLineEdit.text())
        self.settings.setValue("api_key", self.APIKeyLineEdit.text())
        self.settings.setValue("update_interval", self.updateFeedsSpinBox.value())
        self.settings.setValue("feed_filter", self.feedFilterComboBox.currentText())

        self.settings.sync()
        self.close()
